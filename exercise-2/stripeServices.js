const STRIPE_TEST_SECRET_KEY = 'rk_test_51EDa6GIrkMUcWfFwXon9vsJYpTqX2eTqbINAUf4fZC7ivToWv59cAPoHdYhmszwL9ZKWJtUUbaCcHtpjl6rWlXLP00C1dcAoUR'
const stripe = require('stripe')(STRIPE_TEST_SECRET_KEY);
const countries = require('./country');


const saveCustomer = async (customer) => {
  const address = {
    line1: customer.address_line_1,
    country: customer.country
  }
  const name = customer.first_name + ' ' + customer.last_name;
  customer.name = name;

  delete customer.address_line_1;
  delete customer.country;
  delete customer.first_name;
  delete customer.last_name;
  customer.address = address;

  const newCustomer = await stripe.customers.create(customer);
  customer.id = newCustomer.id;
  return {
    email: customer.email,
    customerId: newCustomer.id,
    country: customer.address.country,
  };
}

exports.saveAllCustomers = async (customers) => {
  const countriesByName = await countries.readCountriesByName();
  customers.map(customer => {
    customer.country = countriesByName.get(customer.country);
    return customer;
  })

  let savedCustomers = [];

  for (const customer of customers) {
    const newCustomer = await saveCustomer(customer);
    savedCustomers.push(newCustomer);
  }
  return savedCustomers;
}