const stripeServices = require('./stripeServices');
const customers = require('./customers');

const countryToFilter = 'Spain';

try {
  let finalCustomers = [];
  (async () => {
    const countryCustomers = await customers.readCustomersByCountry(countryToFilter);
    const savedCustomers = await stripeServices.saveAllCustomers(countryCustomers);
    await customers.writeAllCustomers(savedCustomers);
    return savedCustomers;

  })().then(savedCustomers => {
    finalCustomers.push(savedCustomers);
   console.log(finalCustomers);

  });

} catch (e) {
  throw e
}

