const fs = require('fs')


fs.readFile(__dirname + '/countries-ISO3166.json', (error, data) => {
    if(error) {
        throw error;
    }
    const countriesByISO = JSON.parse(data.toString());
    const countresByName = new Map(Object.entries(countriesByISO).map((country) => {
        const iso = country[0];
        const description = country[1];
        return [description, iso];
    }))

    fs.readFile(__dirname + '/customers.json', (error, data) => {
        if(error) {
            throw error;
        }

        const customers = JSON.parse(data.toString());
        customers.filter(customer => customer.country === 'Spain');
        .map(customer => {
            customer.country = countresByName.get(customer.country);
            return customer;
        })
        .forEach(customer => {
            console.log(customer);
        });;

    });

    
});