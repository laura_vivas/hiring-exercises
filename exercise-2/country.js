const fs = require('fs')
const util = require('util');

const asyncReadFile = util.promisify(fs.readFile);

const countriesJson = '/countries-ISO3166.json';

exports.readCountries = async () => {
  const data = await asyncReadFile(__dirname + countriesJson);
  return JSON.parse(data.toString());
}

exports.readCountriesByName = async () => {
  const countriesByIso = await this.readCountries();

  const countriesByName = new Map(Object.entries(countriesByIso).map((country) => {
    const iso = country[0];
    const description = country[1];
    return [description, iso];
  }))

  return countriesByName;
}