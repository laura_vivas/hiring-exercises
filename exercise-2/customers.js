const fs = require('fs')
const util = require('util');

const asyncReadFile = util.promisify(fs.readFile);
const asyncWriteFile = util.promisify(fs.writeFile);

const customerJson = '/customers.json';
const finalCustomersJson = '/final-customers.json';

exports.readAllCustomers = async () => {
  const customerData = await asyncReadFile(__dirname + customerJson);
  return JSON.parse(customerData.toString());
};

exports.readCustomersByCountry = async (country) => {
  const customers = await this.readAllCustomers();
  return customers.filter(customer => customer.country === country);
}

exports.writeAllCustomers = async (customers) => {
  await asyncWriteFile(__dirname + finalCustomersJson, JSON.stringify(customers, null, 2))
    .catch(console.log)
    .then(() => { console.log('Final customers file succesfully written') })
}
