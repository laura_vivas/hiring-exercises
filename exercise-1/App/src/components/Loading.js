import React from 'react';

const Loading = () => {
  return (
    <div 
    className='text-gray-900 font-bold text-md mb-12' 
    className='w-full rounded-lg lg:rounded-r-lg lg:rounded-l-none shadow-2xl bg-white opacity-75 mx-6 lg:mx-0 px-12 h-full overflow-auto'>
      <span>⏳</span> Loading... 
    </div>
  )
}


export default Loading